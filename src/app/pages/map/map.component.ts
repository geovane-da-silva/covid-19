import { Component, OnInit } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { ConvidService } from 'src/app/services/convid.service';
    import { MapsAPILoader, LatLngLiteral } from "@agm/core";



@Component({
  selector: "app-map",
  templateUrl: "map.component.html",
  styleUrls: ['mapa.component.css'],
  

})
export class MapComponent implements OnInit {


  marker:any;
  lat = 18.3892246;
  lng = -66.1305134
  styles = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": "-34"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#0a2047"
            },
            {
                "visibility": "on"
            }
        ]
    }
]

api ={};
  constructor( private convidService:ConvidService) {

    this.convidService.getApiCountries().subscribe( resp =>{
      this.marker = resp;
      console.log('Soy un servicio de maps', this.marker);

      
    this.convidService.getApiBack().subscribe((data:any) =>{
        this.api = data;
        console.log(data);

        
      })

      
    })


  }


  ngOnInit() {

      
         
  }
}
