import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {



  url = 'http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=3d4b24acce7a4a70afdb966d58179591'
  apikey='apiKey=3d4b24acce7a4a70afdb966d58179591'

  constructor( private http:HttpClient) {
    console.log('configurando noticias');
    console.log(this.url )

   }


   getNews(){

   return  this.http.get(this.url);
   }


  
}
