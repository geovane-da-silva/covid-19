import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConvidService {

  constructor( private http:HttpClient) { 
    console.log('Funcionando')
  }

pais:Pais
count:Pais

 //url ="https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/stats?country=us"
 url = 'https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai'
 status = 'jhu-edu/timeseries?onlyCountries=true' // Status de todos los paises
 country = 'jhu-edu/latest?iso2=US'     // EStados Unidos e sus estados
 countries = 'jhu-edu/latest?onlyCountries=true' // todos os paises
 global = 'jhu-edu/brief'
 
 
  urlApi = "http://localhost:3000/usuario";

  getApiConvid(){

  const headers = new HttpHeaders({
    "x-rapidapi-host": "covid-19-coronavirus-statistics.p.rapidapi.com",
	  "x-rapidapi-key": "4f571e2f08msh12e86e257ec5184p16bdd2jsnb412038e090b"
  });

  return this.http.get(`${this.url}`, {
    headers
  })

  }

  getApiBack(){
    return this.http.get(this.urlApi);
  }

  getApiCountry(){

    return this.http.get(`${this.url}/${this.country}`);
  }

  getApiCountries(){

    return this.http.get(`${this.url}/${this.countries}`);
  }

  getApiStatus(){

    return this.http.get(`${this.url}/${this.status}`);
  }


  getApiGlobal(){

    return this.http.get(`${this.url}/${this.global}`);
  }
}

export interface Pais{
  province:string;
  country:string;
  lastUpdate:string;
  confirmed:number;
  deaths:number;
  recovered:number;
  countryregion:any;
  }
  
  