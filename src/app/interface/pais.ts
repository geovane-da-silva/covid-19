export interface Pais{
    provincestate:string,
    countryregion:string;
    lastupdate:string;
    location:number;
    countrycode:string;

    confirmed:number;
    deaths: 1,
    recovered: 42
  }