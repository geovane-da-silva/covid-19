import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { FooterComponent } from "./footer/footer.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { AgmCoreModule } from '@agm/core';
import { NoticiasDetallComponent } from './noticias-detall/noticias-detall.component';

@NgModule({
  imports: [CommonModule, RouterModule, NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD1lNQJ6A8pbI6yM52j6mp3SYfYan1-0SM'
    })
  
  ],
  declarations: [FooterComponent, NavbarComponent, SidebarComponent, NoticiasDetallComponent],
  exports: [FooterComponent, NavbarComponent, SidebarComponent]
})
export class ComponentsModule {}
